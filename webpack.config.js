const path = require('path');
// const webpackTargetElectronRenderer = require('webpack-target-electron-renderer');

module.exports = {

    entry: {
        main: './app/src/mainEntry.js',
        login: './app/src/loginEntry.js'
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'app/dist')
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [
                    'vue-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loader: "file-loader?name=/assets/img/[name].[ext]"
                // loader: "file-loader",
                // options: {
                //     name: "[name].[ext]",
                //     publicPath: path.resolve(__dirname + 'app/dist/assets/img'),
                //     outputPath: path.resolve(__dirname + 'app/dist/assets/img')
                // }
            }
            // {
            //     test: /\.(png|jpg|gif|svg)$/,
            //     loader: 'file-loader',
            //     query: {
            //         name: '[name].[ext]?[hash]'
            //     }
            // }
            // {
            //     test: /\.(jpe?g|png|gif|svg)$/i,
            //     loaders: [
            //         'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
            //         'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
            //     ]
            // }
        ]
    },
    target: 'electron', //!!!!!!!!
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js',
            VueComponents: path.resolve(__dirname, 'app/src/components'),
            Utils: path.resolve(__dirname, 'app/src/utils')
        }
    },
    watch: true
};