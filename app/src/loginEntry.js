/**
 * Created by Julia on 09.01.2018.
 */

    console.log('dir', __dirname);
    import loginReq from 'Utils/requests/loginRequestHandler';
    import electron from 'electron';
    import stylesGeneral from './assets/styles/general.css';
    import stylesLogin from './assets/styles/login.css';

    const remote = electron.remote;

    let form = document.querySelector('form');
    form.addEventListener('submit', function(event) {
        console.log('btn send');
        event.preventDefault();
        const login = form.elements.login.value;
        const password = form.elements.password.value;
        const stayLoggedIn = form.elements.stay_logged_in.checked;
        loginReq(login, password, stayLoggedIn);
    });

    document.addEventListener("DOMContentLoaded", () => {
        console.log('READY');
    });

    // let buttonSignup = document.querySelector('#sign_up');
    // buttonSignup.addEventListener('click', function () {
    //
    //     remote.getCurrentWindow().loadURL(url.format({
    //         pathname: '../app/signup.html',
    //         protocol: 'file:',
    //         slashes: true
    //     }));
    // })




























