'use strict'
const hostURL = require('../config/config').hostURL;
const Store = require('../managers/store.js');
const { remote } = require('electron');
const window = require('electron').BrowserWindow;
const ipcRenderer = require('electron').ipcRenderer;
const url = require('url');
const request = require('request');
const Cache = require('../managers/cache');
const cache = new Cache();

const store = new Store();

module.exports = function (login, password, stayLoggedIn = false) {
    request.post(
        {
            url:hostURL + 'login',
            form: {
                login:login,
                password: password
            }
        },
        function(err, httpResponse, body){
            let win;
            let dialog;
            if(remote) {
                //called if login invoked from the login.html file
                win = remote.getCurrentWindow();
                dialog = remote.dialog;
            } else {
                //called if login invoked from the main.js
                win = window.getFocusedWindow();
                dialog = require('electron').dialog;
            }
            if(err) {
                dialog.showMessageBox(win,
                    {
                        type: 'warning',
                        title: 'Невозможно войти',
                        message: 'Сервер временно недоступен. Попробуйте войти позже'
                    });
                console.log(err);
                //dialog.showErrorBox('Невозможно войти', 'Сервер временно недоступен. Попробуйте войти позже');
                return;
            }
            if (httpResponse.statusCode != 200) {
                console.log(httpResponse.statusCode + ': ' + JSON.parse(body).message);
                dialog.showMessageBox(win,
                    {
                        type: 'warning',
                        title: 'Ошибка входа',
                        message: JSON.parse(body).message
                    });
            } else {
                if(stayLoggedIn) {
                    store.set('authData', { login, password });
                }
                let user = JSON.parse(body).userData;
                user.id = user._id;
                delete user._id;
                if(ipcRenderer) {
                    //called if login invoked from the login.html file
                    ipcRenderer.sendSync('setUser', user);
                } else {
                    //called if login invoked from the main.js
                    cache.set('user', user);
                }
                win.loadURL(url.format({
                    pathname: '../app/index.html',
                    protocol: 'file:',
                    slashes: true
                }));
            }
        });
}
