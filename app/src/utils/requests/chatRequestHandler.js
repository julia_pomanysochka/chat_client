/**
 * Created by Julia on 06.12.2017.
 */

import electron from 'electron';
import request from 'request';
import vuexStore from '../managers/vuexstore';
import config from '../config/config';
import Store from '../managers/store.js';

const hostURL = config.hostURL;
const store = new Store();
const remote = electron.remote;
const window = electron.BrowserWindow;
const ipcRenderer = electron.ipcRenderer;

export let getChats = function() {
    return new Promise((resolve, reject) => {
        let userId = ipcRenderer.sendSync('getUser').id;
        vuexStore.state.currentUser = ipcRenderer.sendSync('getUser'); //?????????????????????????????????????????????????
        request.get(`${hostURL}chats/operatorChats?operatorId=${userId}`,
            function(err, httpResponse, body) {
                let win = remote.getCurrentWindow();
                let dialog = remote.dialog;
                if(err) {
                    dialog.showMessageBox(win,
                        {
                            type: 'warning',
                            title: 'Ошибка сервера',
                            message: 'Сервер временно недоступен. Попробуйте войти позже'
                        });
                    console.log(err);
                    reject(new Error('Server error'));
                }
                if (httpResponse.statusCode != 200) {
                    dialog.showMessageBox(win,
                        {
                            type: 'warning',
                            title: 'Ошибка сервера',
                            message: 'Невозможно получить данные о чатах'
                        });
                    reject(new Error('Server error'));
                } else {
                    resolve(JSON.parse(body));
                }
            });
    });
}

export let clearMissedMessages = function () {
    console.log('clearMissedMessages');
    return new Promise((resolve, reject) => {
        let userId = ipcRenderer.sendSync('getUser').id;
        let chatId = vuexStore.state.selectedChat.id;
        request.post(`${hostURL}chats/clearMissed`,
            { "operatorId": userId, "chatId": chatId },
            function(err, httpResponse, body) {
                let win = remote.getCurrentWindow();
                let dialog = remote.dialog;
                if(err) {
                    dialog.showMessageBox(win,
                        {
                            type: 'warning',
                            title: 'Ошибка сервера',
                            message: 'Сервер временно недоступен. Попробуйте войти позже'
                        });
                    console.log(err);
                    reject(new Error('Server error'));
                }
                console.log('httpResponse.statusCode', httpResponse.statusCode);
                if (httpResponse.statusCode != 200) {
                    dialog.showMessageBox(win,
                        {
                            type: 'warning',
                            title: 'Ошибка сервера',
                            message: 'Невозможно получить данные о чатах'
                        });
                    reject(new Error('Server error'));
                } else {
                    resolve(JSON.parse(body));
                }
            });
    });
}


export let createChat = function () {
    console.log('createChat');
}

