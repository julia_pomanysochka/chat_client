'use strict'

module.exports = {
    hostURL: 'http://localhost:3000/v1/',
    storeConfig: {
        configName: 'user-preferences',
        defaults: {
            authData: {login: '', password: ''},
            windowBounds: { width: 800, height: 600 }
        }
    }
}