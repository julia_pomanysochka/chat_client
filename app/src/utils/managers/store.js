/**
 * Created by Julia on 27.11.2017.
 */
const electron = require('electron');
const path = require('path');
const fs = require('fs');
const opts = require('../config/config').storeConfig;

let instance = null;

class Store {

    constructor() {
        if(!instance) {
            instance = this;
            const userDataPath = (electron.app || electron.remote.app).getPath('userData');
            this.path = path.join(userDataPath, opts.configName + '.json');
            this.data = parseDataFile(this.path, opts.defaults);
        }
        return instance;
    }

    get(key) {
        return this.data[key];
    }

    set(key, val) {
        this.data[key] = val;
        try {
            fs.writeFileSync(this.path, JSON.stringify(this.data));
        } catch(err) {
            
            console.log('Can not write', val, 'to', key, ':', err);
        }

    }
}

function parseDataFile(filePath, defaults) {
    // try/catch it in case the file doesn't exist yet, which will be the case on the first application run.
    // `fs.readFileSync` will return a JSON string which we then parse into a Javascript object
    try {
        return JSON.parse(fs.readFileSync(filePath));
    } catch(error) {
        // if there was some kind of error, return the passed in defaults instead.
        return defaults;
    }
}

module.exports = Store;