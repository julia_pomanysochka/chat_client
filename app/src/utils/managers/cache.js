/**
 * Created by Julia on 20.12.2017.
 */
let instance = null;

class Cache {
    constructor() {
        if(!instance) {
            instance = this;
            this.user = null;
        }
        return instance;
    }

    get(key) {
        //console.log('get', this[key]);
        return this[key];
    }

    set(key, val) {
        console.log('setting', val, 'to', key);
        this[key] = val;
    }
}

module.exports = Cache;