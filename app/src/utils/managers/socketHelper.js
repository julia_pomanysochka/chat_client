/**
 * Created by Julia on 18.01.2018.
 */
import electron from 'electron';
import store from './vuexstore';
const host = "ws://127.0.0.1:8080";
const ipcRenderer = electron.ipcRenderer;
// let chats = null;
const FROM_REGEXP = /<from:(.+):>(.+)/;

let socket = undefined; //вебсокет сервер


export function sendMessage (from, chatId, text) {
    console.log('text in sendMessage', text);
    if(from && chatId && text) {
        let msg = {
            event: 'send message',
            data: {
                from: from,
                chatId: chatId,
                date: Date.now,
                type: 'text',
                message: text
            }
        }
        socket.send(JSON.stringify(msg));
        //addText(`from ${from} to ${to}: ${text}`);
    }
}

export function sendChatActivatedEvent(from, chatId) {
    if(from && chatId) {
        let msg = {
            event: 'chat activated',
            data: {
                from: from,
                chatId: chatId
            }
        }
        socket.send(JSON.stringify(msg));
    }
}





export function newSocket () {
    if (socket !== undefined) socket.close();
    // addText(`Connecting to ${host} ...`, 'system');
    socket = new WebSocket(host);

    socket.onopen = function () {
        const user = ipcRenderer.sendSync('getUser');
        socket.send(JSON.stringify({
            event: 'login',
            data: {
                id: user.id,
                name: user.name,
                region: user.region,
                phone: user.contactInfo.phone,
                email: user.contactInfo.email,
                login: user.auth.login,
                type: user.role.type,
                specializations: user.role.specializations
            }
        }));
        // addText('Connection established', 'system');
    };

    //при отключении - выдать что отключен
    socket.onclose = function () {
        // addText('Connection lost...', 'system');
        newSocket ();
    };

    //обработка сообщения из сокета
    socket.onmessage = function (message) {
        let response = JSON.parse(message.data);
        let event = response.event;
        switch (event) {
            case 'login response':
                let operators = response.data.onlineOperatorsList;
                operators.forEach(function each(operator) {
                    store.commit('addOnlineOperator', operator);
                });
                break;
            case 'user connected':
                let connectedOperator = response.data.user;
                store.commit('addOnlineOperator', connectedOperator);
                break;
            case 'user disconnected':
                let disconnectedOperator = response.data.user;
                store.commit('removeOnlineOperator', disconnectedOperator);
                break;
            case 'receive message':
            case 'send message response':
                let msg = response.data;
                msg.id = ID();
                addMessageToVuex(msg);
                break;
            case 'mark as read':
                console.log('mark as read');
                let data = response.data;
                store.commit('clearMissedMessages',
                    {chatId: data.chatId,
                        memberId: data.from});
                break;
            default:
                console.log('unknown message');
        }
    };
}

export function addMessageToVuex(msg) {
    let chatId = msg.chat;
    let srcSearch = msg.message.match(FROM_REGEXP);
    let src = '';
    let text = '';
    if(srcSearch) {
        src = srcSearch[1];
        text = srcSearch[2];
    } else {
        text = msg.message;
    }
    let message = {
        message: text,
        type: msg.type,
        sender: msg.from,
        missed: msg.missed,
        id: msg.id,
        source: src
    };
    console.log('INCOMING MSG', message);
    store.commit('addMessageToChat',
        {chatId: chatId,
            message: message});
}




//????????????????????????

var ID = function () {
    return '_' + Math.random().toString(36).substr(2, 9);
};

