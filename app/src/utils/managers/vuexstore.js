/**
 * Created by Julia on 20.12.2017.
 */
import * as chatRequestHandler from '../requests/chatRequestHandler';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        currentUser: {},
        onlineOperators: [],
        onlineOperatorsId: [],
        chats: [],
        selectedChat: '',
        // selectedChatFull: '',
        selectedMessages: [],
        forward: false
    },
    getters: {
        currentUserId: state => {
            return state.currentUser.id;
        },
        getChatMemberName: (state) => (idMember, idChat) => {
            // console.log('idMember', idMember);
            // console.log('idChat', idChat);
            // console.log('chats', state.chats);
            for(let chat of state.chats) {
                if(chat.id === idChat) {
                    for(let member of chat.members) {
                        // console.log('chat.member', member);
                        if(member.id === idMember) {
                            return member.name;
                        }
                    }
                }
            }
        }
        // getOperatorNameById: (state) => (id) => {
        //     console.log('ID', id);
        //     console.log('onlineOperators', state.onlineOperators);
        // }
    },
    mutations: {
        addOnlineOperator(state, operator) {
            state.onlineOperators.push(operator);
            state.onlineOperatorsId.push(operator.id);
        },
        removeOnlineOperator(state, operator) {
            state.onlineOperators.forEach((item, i) => {
                if (operator.id === item.id) {
                    state.onlineOperators.splice(i,1);
                }
            });
            state.onlineOperatorsId.forEach((id, i) => {
                if (operator.id === id) {
                    state.onlineOperatorsId.splice(i,1);
                }
            });
        },
        setChats(state, chats) {
            for(let chat of chats) {
                state.chats.push(chat);
            }
        },
        setSelectedChat(state, chat) {
            state.selectedChat = chat;
        },
        addMessageToChat(state, arg) {
            let chatId = arg.chatId;
            for(let chat of state.chats) {
                if(chat.id === chatId) {
                    chat.messages.push(arg.message);
                    console.log('state.currentUser.id', state.currentUser.id);
                    console.log('arg.message.missed', arg.message.missed);
                    if(arg.message.missed.indexOf(state.currentUser.id) > -1) {
                        chat.missedCount++;
                    }
                }
            }
        },
        selectMessage(state, arg) {
            state.selectedMessages.push(arg);
        },
        deselectMessage(state, arg) {
            state.selectedMessages.forEach((message, i) => {
                if(message.id === arg.id) {
                    state.selectedMessages.splice(i, 1);
                }
            });
        },
        clearSelectedMessages(state) {
            state.selectedMessages = [];
        },
        setForward(state) {
            if(state.selectedMessages.length > 0) {
                state.forward = true;
            }
        },
        unsetForward(state) {
            state.forward = false;
        },
        clearMissedMessages(state, arg) {
            let chatId = arg.chatId;
            let memberId = arg.memberId;
            for (let chat of state.chats) {
                if (chat.id === chatId) {
                    let i = chat.messages.length - 1;
                    while (true) {
                        let message = chat.messages[i];
                        if (message.missed) {
                            let index = message.missed.indexOf(memberId);
                            if (index > -1) {
                                message.missed.splice(index, 1);
                            }
                        } else {
                            break;
                        }
                        i--;
                        break;
                    }
                    if(memberId === state.currentUser.id) {
                        chat.missedCount = 0;
                    }
                }
            }
        }



    }
    // actions: {
    //     addChatWithMsg(context, selectedChatId) {
    //         chatRequestHandler.getChat(selectedChatId).then(chat => context.commit('addChatWithMsg', chat))
    //             .catch(error => console.log('Some error', error));
    //     }
    // }
});





