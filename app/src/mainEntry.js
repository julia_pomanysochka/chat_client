/**
 * Created by Julia on 09.01.2018.
 */

import Vue from 'vue'
import App from './App.vue'

import autosize from 'autosize';
import store from './utils/managers/vuexstore';
import * as chatRequestHandler from './utils/requests/chatRequestHandler';
import * as socketHelper from './utils/managers/socketHelper';
import stylesGeneral from './assets/styles/general.css';
import stylesMain from './assets/styles/main.css';

// console.log('chatRequestHandler in mainEntry');
// console.log('main Entry after import');








/* узнать текущее время */
// function showtime() {
//     var time = new Date(Date.now()),
//         h = time.getHours(),
//         m = time.getMinutes(),
//         s = time.getSeconds(),
//         d = time.getMilliseconds();
//     if (h < 10) h = '0' + h;
//     if (m < 10) m = '0' + m;
//     if (s < 10) s = '0' + s;
//     if (d < 10) d = '00' + d;
//     else if (d < 100) d = '0' + d;
//     return h + ':' + m + ':' + s + '.' + d;
// }

/* при загрузке окна подключиться к сокету и определить методы работы */
window.onload = function () {
    // console.log('window onload');
    socketHelper.newSocket();
    chatRequestHandler.getChats().then(chats => store.commit('setChats', chats));
    autosize(document.querySelector('textarea'));
};

new Vue({
    el: '#app',
    render: h => h(App)
})

