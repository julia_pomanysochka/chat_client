'use strict'
/**
 * Created by Julia on 27.11.2017.
 */

// import path from 'path';
// import url from 'url';
// import request from 'request';
// import electron from 'electron';
// import config from './app/src/utils/config/config';
// import Store from './app/src/utils/managers/store.js';
// import loginReq from './app/src/utils/requests/loginRequestHandler';
// import Cache from './app/src/utils/managers/cache';
//
//
// const app = electron.app;
// const BrowserWindow = electron.BrowserWindow;
// const ipcMain = electron.ipcMain;
// const hostURL = config.hostURL;
// const cache = new Cache();
// import {enableLiveReload} from 'electron-compile';

const path = require('path');
const url = require('url');
const Store = require('./app/src/utils/managers/store.js');
const electron = require('electron');
console.log('electron', electron);
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipcMain = electron.ipcMain;

const config = require('./app/src/utils/config/config');
const hostURL = config.hostURL;

const loginReq = require('./app/src/utils/requests/loginRequestHandler');
const request = require('request');
const Cache = require('./app/src/utils/managers/cache');
const cache = new Cache();


let win;

// enableLiveReload();

// First instantiate the class
const store = new Store();

function createWindow () {
    console.log('store = ', store);
    //creating window and managing window size
    let { width, height } = store.get('windowBounds');
    win = new BrowserWindow({width: width, height: height});
    win.webContents.openDevTools();
    win.on('resize', () => {
        let { width, height } = win.getBounds();
        store.set('windowBounds', { width, height });
    });

    win.on('closed', () => {
        win = null;

    });
    if (process.env.NODE_ENV !== 'production') {
        require('vue-devtools').install()
    }


    //authentication
    console.log('store = ', store);

    let { login, password } = store.get('authData');
    if (!(login && password)) {
        //загрузить страничку с аутентификацией
        win.loadURL(url.format({
            pathname: '../app/login.html',
            protocol: 'file:',
            slashes: true
        }))
    } else {
        loginReq(login, password, true);
    }

}
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
});

ipcMain.on('setUser', (event, arg) => {
    cache.set('user', arg);
    event.returnValue = null;
});

ipcMain.on('getUser', (event, arg) => {
    event.returnValue = cache.get('user');
});

// ipcMain.on('setOnlineOperators', (event, arg) => {
//     console.log('setOnlineOperators', arg);
//     cache.set('onlineOperators', arg);
//     event.returnValue = null;
// });
//
// ipcMain.on('getOnlineOperators', (event, arg) => {
//     console.log('getOnlineOperators');
//     event.returnValue = cache.get('onlineOperators');
// });
//
// ipcMain.on('addOnlineOperator', (event, arg) => {
//     cache.addOnlineOperator(arg);
//     event.returnValue = null;
// });
//
// ipcMain.on('removeOnlineOperator', (event, arg) => {
//     cache.removeOnlineOperator(arg)
//     event.returnValue = null;
// });

// import App from './components/App.vue';
// const App = require('./components/App.vue');
//
// new Vue({
//     el: '#app',
//     components: {App},
//     // Attach the Vue instance to the window,
//     // so it's available globally.
//     // created: function () {
//     //     window.Vue = this
//     // }
// })






